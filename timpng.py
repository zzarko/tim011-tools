#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    TimPng (c) 2022  Žarko Živanov

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import division

VERSION="1.0"

import sys
import os
import argparse
import array
import png      # python-png / python3-png / pypng

###############################################
#               Common functions
###############################################

# returns the size of an open file
def flen(f):
    return os.fstat(f.fileno()).st_size

# prints error message and exits the program
def error(s):
    print("\nERROR: {0}\n".format(s))
    exit(1)

###############################################
#           Argument parser settings
###############################################
about = """TimPng %s (c) 2022  Zarko Zivanov

This program is used to convert PNG into TIM-011
TPC (TIM Picture) files, for usage with showPic
function.

Max input image size is 512x252 pixels and it
must have max 4 different colors (shades of gray).
Picture will be cropped to 4 pixels boundary.

For each PNG file a separate '.pic' file will be
created.""" % (VERSION)

epilog="""
Steps when using GIMP to convert the image:
- Image->Mode->Grayscale - convert to grayscale
- Resize/Crop image to be max 512x252. Take into
  account that TIM pixels aren't square
- Image->Mode->Indexed - convert to 4-color image
  by setting "Maximum number of colors" to 4
- Save image as PNG, "8bpc GRAY"

TPC file format (1 block is 4x4 pixels):
    1 byte block width
    1 byte block height
    image data organized by 4-pixel wide columns
"""

parser = argparse.ArgumentParser(description=about, formatter_class=argparse.RawDescriptionHelpFormatter,epilog=epilog)
parser.add_argument('file', metavar='file', nargs='*', default="", help='(path to) PNG file')

# parse command line
args = parser.parse_args()

# recognised colors - all others will be color 00 - black
colors = {0x00:0x00, 0x40:0x01, 0x80:0x02, 0xFF:0x03}

if len(args.file) == 0:
    error("At least one PNG file name is required. Add '-h' for help.")


for pngFile in args.file:
    if not os.path.exists(pngFile):
        error("File '%s' not found!" % pngFile)
    if not pngFile[-4:] in [".png", ".PNG"]:
        error("Image '%s' must have a 'png' extension!" % pngFile)

    # read PNG file
    pngObject = png.Reader(pngFile)
    pngDirect = pngObject.asDirect()
    tileMatrix = list(pngDirect[2])
    tilesH = len(tileMatrix)
    tilesW = len(tileMatrix[0])
    print("PNG size:", tilesW, "x", tilesH)
    if (tilesW > 512) or (tilesH > 252):
        print("Max dimensions are 512x252")
        exit(1)

    # extract colors
    cols = []
    for y in range(tilesH):
        for x in range(tilesW):
            col = tileMatrix[y][x]
            if col not in cols:
                cols.append(col)
    if len(cols) > 4:
        print("Max number of colors is 4, your picture has", len(cols))
        exit(1)
    cols.sort()
    colors = {}
    for i,col in enumerate(cols):
        colors[col] = i;
    print("Color intensities:",cols)
    
    width = tilesW // 4
    height = tilesH // 4

    print("Block size:", width, "x", height, "(pixel size", width*4, "x", height*4, ")")
    print("Max [X,Y] coordinates:", 128-width, ",", 64-height)

    picture = array.array('B')
    picture.append(width);
    picture.append(height);
    for xx in range(width):
        for y in range(height*4):
            byte = 0
            for x in range(xx*4+3,xx*4-1,-1):
                col = colors[tileMatrix[y][x]]
                byte <<= 2
                byte |= col
            picture.append(byte)
    
    # open ZSM file for writing
    picName = "%s.tpc" % pngFile[:-4]
    picFile = open(picName, "wb")
    picFile.write(picture)
    picFile.close()

